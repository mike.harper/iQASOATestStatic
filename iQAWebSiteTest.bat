@echo off

echo Commencing Import
soatestcli -import . -data .

echo Commencing LinksCheck Test
soatestcli -config TestAssets/configs/LinksCheck.properties -environment "Test Environment" -report reports/linkscheckresults.xml -data .

echo Commencing SpellingCheck Test
soatestcli -config TestAssets/configs/SpellingCheck.properties -environment "Test Environment" -report reports/spellingcheckresults.xml -data .

echo Commencing WebStandardsCheck Test
soatestcli -config TestAssets/configs/WebStandardsCheck.properties -environment "Test Environment" -report reports/webstandardscheckresults.xml -data .