<?xml version="1.0" encoding="UTF-8"?>
<SOAtestProject xmlVersion="5" productVersion="9.9.5">
 <TestSuite className="webtool.test.TestSuite" version="43.15.2">
  <DebugAssets className="webtool.test.debug.DebugAssets" version="1">
  </DebugAssets>
  <ReportingSettings className="webtool.test.ReportingSettings" version="1.1">
   <TestGRSSettings className="webtool.test.grs.TestGRSSettings" version="1.4.3">
   </TestGRSSettings>
  </ReportingSettings>
  <environmentConfig>true</environmentConfig>
  <EnvironmentConfiguration className="webtool.environment.EnvironmentConfiguration" version="1">
   <environmentsSize>1</environmentsSize>
   <EnvironmentReference className="webtool.environment.EnvironmentReference" version="1.1">
    <name>Environment Reference</name>
   </EnvironmentReference>
  </EnvironmentConfiguration>
  <enabled>true</enabled>
  <name>Test Suite</name>
  <TestSuitePerformanceOptions className="webtool.test.performance.TestSuitePerformanceOptions" version="1.5">
   <APMPerformanceProfileProviderContainer className="webtool.test.performance.apm.APMPerformanceProfileProviderContainer" version="1.1">
   </APMPerformanceProfileProviderContainer>
   <profiless size="0">
   </profiless>
  </TestSuitePerformanceOptions>
  <rootTestSuite>true</rootTestSuite>
  <AdvancedExecutionOptions className="webtool.test.AdvancedExecutionOptions" version="1.3">
  </AdvancedExecutionOptions>
  <TestSuiteLoadTestOptions className="webtool.loadtest.sim.component.soatest.TestSuiteLoadTestOptions" version="1.1">
  </TestSuiteLoadTestOptions>
  <SetupTeardownOptions className="webtool.test.SetupTeardownOptions" version="1.1">
  </SetupTeardownOptions>
  <browserOptions>true</browserOptions>
  <TestSuiteBrowserTestingOptions className="webtool.test.TestSuiteBrowserTestingOptions" version="1.14">
   <DefaultCustomFrameworkOption className="webtool.tool.DefaultCustomFrameworkOption" version="1.1.1">
    <useDefault>false</useDefault>
    <useWebDriver>false</useWebDriver>
   </DefaultCustomFrameworkOption>
   <InheritingAuthentication className="webtool.test.InheritingAuthentication" version="1.4">
   </InheritingAuthentication>
   <DefaultCustomHeadlessOption className="webtool.tool.DefaultCustomHeadlessOption" version="1.1.1">
   </DefaultCustomHeadlessOption>
   <DefaultCustomBrowserType className="webtool.tool.DefaultCustomBrowserType" version="1.8.1">
    <useDefault>false</useDefault>
    <SingleBrowserType className="com.parasoft.util.SingleBrowserType" version="1.1.1">
     <typeValue>0</typeValue>
    </SingleBrowserType>
   </DefaultCustomBrowserType>
  </TestSuiteBrowserTestingOptions>
  <TestSuiteSoapClientOptions className="webtool.test.TestSuiteSoapClientOptions" version="1.2">
   <WsdlEndpointOptions className="webtool.tool.WsdlEndpointOptions" version="1.1">
    <DefaultCustomWsdl className="webtool.tool.DefaultCustomWsdl" version="1.1.1">
    </DefaultCustomWsdl>
    <DefaultCustomEndpoint className="webtool.tool.DefaultCustomEndpoint" version="1.1.1">
    </DefaultCustomEndpoint>
   </WsdlEndpointOptions>
   <SOAPRPCToolOptions className="webtool.app.SOAPRPCToolOptions" version="1.5">
    <ResetExistingCookiesPreferenceProviderImpl className="webtool.tool.ResetExistingCookiesPreferenceProviderImpl" version="1.1">
    </ResetExistingCookiesPreferenceProviderImpl>
    <DefaultResetExistingCookies className="webtool.tool.DefaultResetExistingCookies" version="1.1.1">
    </DefaultResetExistingCookies>
    <RequestHeaderConstrainProviderImpl className="webtool.tool.RequestHeaderConstrainProviderImpl" version="1.1">
    </RequestHeaderConstrainProviderImpl>
    <DefaultCustomConstrain className="webtool.tool.DefaultCustomConstrain" version="1.1.1">
    </DefaultCustomConstrain>
    <DefaultCustomEncoding className="webtool.tool.DefaultCustomEncoding" version="1.1.1">
    </DefaultCustomEncoding>
    <DefaultCustomTimeout className="webtool.tool.DefaultCustomTimeout" version="1.2.1">
    </DefaultCustomTimeout>
    <DefaultCustomSoapVersion className="webtool.tool.DefaultCustomSoapVersion" version="1.1.1">
    </DefaultCustomSoapVersion>
    <DefaultCustomAttachment className="webtool.tool.DefaultCustomAttachment" version="1.1.1">
    </DefaultCustomAttachment>
    <DefaultCustomTransport className="webtool.tool.DefaultCustomTransport" version="1.1.1">
    </DefaultCustomTransport>
   </SOAPRPCToolOptions>
  </TestSuiteSoapClientOptions>
  <profileMappingID>0</profileMappingID>
  <maxProfileMappingID>1</maxProfileMappingID>
  <PropertyOwnerImpl className="com.parasoft.property.PropertyOwnerImpl" version="1.1">
  </PropertyOwnerImpl>
  <nextIdentifier>13</nextIdentifier>
  <testsSize>1</testsSize>
  <ToolTest className="webtool.test.ToolTest" version="4.15.2">
   <DebugAssets className="webtool.test.debug.DebugAssets" version="1">
   </DebugAssets>
   <ReportingSettings className="webtool.test.ReportingSettings" version="1.1">
    <TestGRSSettings className="webtool.test.grs.TestGRSSettings" version="1.4.3">
    </TestGRSSettings>
   </ReportingSettings>
   <testLogic>true</testLogic>
   <TestLogic className="webtool.test.logic.TestLogic" version="1.7">
    <TestExecutionDelay className="webtool.test.logic.TestExecutionDelay" version="1.2">
    </TestExecutionDelay>
    <TestExecutionDelay className="webtool.test.logic.TestExecutionDelay" version="1.2">
    </TestExecutionDelay>
    <condition>true</condition>
    <TestLogicVariableCondition className="webtool.test.logic.TestLogicVariableCondition" version="1.2">
    </TestLogicVariableCondition>
    <TestDependencyLogic className="webtool.test.logic.TestDependencyLogic" version="1.2">
    </TestDependencyLogic>
   </TestLogic>
   <testID>2</testID>
   <enabled>true</enabled>
   <name>Scanning Tool</name>
   <ScanningTool className="webtool.site.ScanningTool" version="v.2.4.14">
    <iconName>scanWebResources</iconName>
    <name>Scanning Tool</name>
    <ScanningOptions className="webtool.site.ScanningOptions" version="1.3">
     <followNonrestrictedRedirects>true</followNonrestrictedRedirects>
     <urlRestrictionsSize>1</urlRestrictionsSize>
     <UrlRestriction className="webtool.project.UrlRestriction" version="1.1">
      <url>http(s)://www.integrationqa.com/*</url>
      <positive>true</positive>
     </UrlRestriction>
    </ScanningOptions>
    <WebSiteCollectionManipulator className="webtool.site.WebSiteCollectionManipulator" version="1.1">
     <sitesSize>1</sitesSize>
     <WebSite className="webtool.site.WebSite" version="26">
      <targets size="1">
       <currentTarget index="0">true</currentTarget>
       <TargetLocation className="webtool.site.TargetLocation" index="0" version="9.18">
        <followLinks>true</followLinks>
        <invokeMultipleCGI>true</invokeMultipleCGI>
        <passCookies>true</passCookies>
        <caseSensitive>true</caseSensitive>
        <ignoreRobots>true</ignoreRobots>
        <httpDefault>*</httpDefault>
        <url>rO0ABXA=</url>
        <booleanFilters>rO0ABXNyABBqYXZhLnV0aWwuVmVjdG9y2Zd9W4A7rwEDAANJABFjYXBhY2l0eUluY3JlbWVudEkADGVsZW1lbnRDb3VudFsAC2VsZW1lbnREYXRhdAATW0xqYXZhL2xhbmcvT2JqZWN0O3hwAAAAAAAAAAB1cgATW0xqYXZhLmxhbmcuT2JqZWN0O5DOWJ8QcylsAgAAeHAAAAAKcHBwcHBwcHBwcHg=</booleanFilters>
        <name>Default</name>
       </TargetLocation>
      </targets>
      <loadingDepth>0</loadingDepth>
      <startingURLRef>true</startingURLRef>
      <NamedReference className="webtool.site.NamedReference" version="3.1">
       <type>4</type>
       <id>https://www.integrationqa.com</id>
      </NamedReference>
      <id>-530989897</id>
      <cookiesSize>20</cookiesSize>
      <Cookie className="com.parasoft.net.Cookie" version="2">
       <name>wp_locale_test_group</name>
       <value>hct1</value>
       <domain>wordpress.com</domain>
       <path>/</path>
       <expiration>1512768751000</expiration>
      </Cookie>
      <Cookie className="com.parasoft.net.Cookie" version="2">
       <name>__cfduid</name>
       <value>d663b7ca0fdb3e5c54b2bc71f4efab2ec1481232867</value>
       <domain>.addtoany.com</domain>
       <path>/</path>
       <expiration>1512768867000</expiration>
      </Cookie>
      <Cookie className="com.parasoft.net.Cookie" version="2">
       <name>page_services</name>
       <value>email</value>
       <domain>.addtoany.com</domain>
       <path>/</path>
       <expiration>1796592867000</expiration>
      </Cookie>
      <Cookie className="com.parasoft.net.Cookie" version="2">
       <name>visid_incap_169238</name>
       <value>OQX0sgzZQB2C4r7SluPNkcjRSVgAAAAAQUIPAAAAAADJkQLk1wB3q2MWPw/RT2rJ</value>
       <domain>.education.govt.nz</domain>
       <path>/</path>
       <expiration>1512719165000</expiration>
      </Cookie>
      <Cookie className="com.parasoft.net.Cookie" version="2">
       <name>visid_incap_168768</name>
       <value>jkyTiL4/TZ+VsUV+TXW0R8nRSVgAAAAAQUIPAAAAAACj1Ifs8f9lu18t9NRgCRUb</value>
       <domain>.dia.govt.nz</domain>
       <path>/</path>
       <expiration>1512727754000</expiration>
      </Cookie>
      <Cookie className="com.parasoft.net.Cookie" version="2">
       <name>SC_ANALYTICS_GLOBAL_COOKIE</name>
       <value>369c1585e27d43d0afa8b3fb6ade890d|False</value>
       <domain>contact.co.nz</domain>
       <path>/</path>
       <expiration>1796765621000</expiration>
      </Cookie>
      <Cookie className="com.parasoft.net.Cookie" version="2">
       <name>visid_incap_680755</name>
       <value>n4GAmIIsR4GGZvQoUuJ23szRSVgAAAAAQUIPAAAAAABo5LWyCIeEZEd0+VNtEvVU</value>
       <domain>.justice.govt.nz</domain>
       <path>/</path>
       <expiration>1512719165000</expiration>
      </Cookie>
      <Cookie className="com.parasoft.net.Cookie" version="2">
       <name>EPi_NumberOfVisits</name>
       <value>1,2016-12-08T21:34:07</value>
       <domain>saab.com</domain>
       <path>/</path>
       <expiration>253402300799000</expiration>
      </Cookie>
      <Cookie className="com.parasoft.net.Cookie" version="2">
       <name>CMSPreferredCulture</name>
       <value>en-NZ</value>
       <domain>www.vector.co.nz</domain>
       <path>/</path>
       <expiration>1512768852000</expiration>
      </Cookie>
      <Cookie className="com.parasoft.net.Cookie" version="2">
       <name>CMSPreferredCulture</name>
       <value>en-NZ</value>
       <domain>datacom.co.nz</domain>
       <path>/</path>
       <expiration>1512768852000</expiration>
      </Cookie>
      <Cookie className="com.parasoft.net.Cookie" version="2">
       <name>CurrentContact</name>
       <value>99fd36a3-6b46-42e3-a5cc-0c17d7380d29</value>
       <domain>datacom.co.nz</domain>
       <path>/</path>
       <expiration>3059069652000</expiration>
      </Cookie>
      <Cookie className="com.parasoft.net.Cookie" version="2">
       <name>VisitorStatus</name>
       <value>11060281274</value>
       <domain>datacom.co.nz</domain>
       <path>/</path>
       <expiration>2112384852000</expiration>
      </Cookie>
      <Cookie className="com.parasoft.net.Cookie" version="2">
       <name>__cfduid</name>
       <value>d8507652f220b389eb377193f363a18311481232862</value>
       <domain>.thalesgroup.com</domain>
       <path>/</path>
       <expiration>1512768862000</expiration>
      </Cookie>
      <Cookie className="com.parasoft.net.Cookie" version="2">
       <name>guest_id</name>
       <value>v1%3A148123286361731206</value>
       <domain>.twitter.com</domain>
       <path>/</path>
       <expiration>1544304863000</expiration>
      </Cookie>
      <Cookie className="com.parasoft.net.Cookie" version="2">
       <name>_mobile_sess</name>
       <value>BAh7ByIKZmxhc2hJQzonQWN0aW9uQ29udHJvbGxlcjo6Rmxhc2g6OkZsYXNoSGFzaHsABjoKQHVzZWR7ADoQX2NzcmZfdG9rZW4iJTJmYjViZDFmMDkzYWM5N2FlYThiNjJmYmQwOWFkNTNh--19afbf39ceb17e815122a086c0c9db61e590ad8a</value>
       <domain>.twitter.com</domain>
       <path>/</path>
       <secure>true</secure>
       <expiration>1486416863000</expiration>
      </Cookie>
      <Cookie className="com.parasoft.net.Cookie" version="2">
       <name>d</name>
       <value>32</value>
       <domain>.twitter.com</domain>
       <path>/</path>
       <secure>true</secure>
       <expiration>1512768863000</expiration>
      </Cookie>
      <Cookie className="com.parasoft.net.Cookie" version="2">
       <name>mobile_metrics_token</name>
       <value>148123286356090213</value>
       <domain>.twitter.com</domain>
       <path>/</path>
       <secure>true</secure>
       <expiration>1544304863000</expiration>
      </Cookie>
      <Cookie className="com.parasoft.net.Cookie" version="2">
       <name>zrca</name>
       <value>5</value>
       <domain>.twitter.com</domain>
       <path>/</path>
       <secure>true</secure>
       <expiration>1483824863000</expiration>
      </Cookie>
      <Cookie className="com.parasoft.net.Cookie" version="2">
       <name>bcookie</name>
       <value>&quot;v=2&amp;d32d0efa-0adf-425c-881a-2fe37dfedd50&quot;</value>
       <domain>.linkedin.com</domain>
       <path>/</path>
       <expiration>1544346717000</expiration>
      </Cookie>
      <Cookie className="com.parasoft.net.Cookie" version="2">
       <name>bscookie</name>
       <value>&quot;v=1&amp;20161208213424bbd6a3c2-746a-4d58-8c3f-c43e9cd26e82AQE-ImUJ84ZeuwbZI8qY8tehcnCbKURg&quot;</value>
       <domain>.www.linkedin.com</domain>
       <path>/</path>
       <secure>true</secure>
       <expiration>1544346717000</expiration>
      </Cookie>
      <dir>true</dir>
      <WebRoot className="webtool.site.WebRoot" version="14.5.1">
       <type>7</type>
       <name>.</name>
       <childrenSize>1</childrenSize>
       <WebOther className="webtool.site.WebOther" version="2.14.5">
        <type>8</type>
        <dataSize>3</dataSize>
        <keyID>1</keyID>
        <BooleanAuxData className="webtool.site.BooleanAuxData" version="1">
         <value>true</value>
        </BooleanAuxData>
        <keyID>12</keyID>
        <FormData className="webtool.form.FormData" version="1.1">
         <formsSize>2</formsSize>
         <Form className="webtool.form.Form" version="13">
          <pageEncoding>UTF-8</pageEncoding>
          <tag>&lt;form role=&quot;search&quot; method=&quot;get&quot; class=&quot;et-search-form&quot; action=&quot;https://www.integrationqa.com/&quot;&gt;</tag>
          <name>Anonymous</name>
          <action>https://www.integrationqa.com/</action>
          <elementsSize>1</elementsSize>
          <InputText className="webtool.form.InputText" version="4.6.8">
           <externalID>9</externalID>
           <tag>AgAAAJCol/t4RqGm2yC+09pUPm/ys2S2qtFEkeVXdW60dNXX+HX9Ky2/6uV+g3n8l2ytZNVS0L7Tlf9Ak7eiTeJkM92rzU/AXQKb/FAaTc+N56Lzv6LfjCfp81vdBiqAVZAmcQ1IeFiQRoLUK2v1HPIJ4b6JiTjX9V5iGdB+qIEwFfatSSTdJOkwCrsLp16ZsvZnEdY=</tag>
           <beginLine>240</beginLine>
           <endLine>240</endLine>
          </InputText>
         </Form>
         <Form className="webtool.form.Form" version="13">
          <pageEncoding>UTF-8</pageEncoding>
          <tag>&lt;form action=&quot;/#wpcf7-f561-p109-o1&quot; method=&quot;post&quot; class=&quot;wpcf7-form&quot; novalidate=&quot;novalidate&quot;&gt;</tag>
          <name>Anonymous$$2</name>
          <action>/#wpcf7-f561-p109-o1</action>
          <elementsSize>10</elementsSize>
          <InputHidden className="webtool.form.InputHidden" version="1.2.8">
           <externalID>3</externalID>
           <tag>AgAAAFCsF+2NSiL5s2IHoA8hq6it7dshbWP2b6bMCS7XOBqFx3LiUmJR+0Rka734DBFQXB+/+mmJz8XGSf4nLlwSCKEeeh/jfa8AyYQqyqFUWD4zZg==</tag>
           <beginLine>781</beginLine>
           <endLine>781</endLine>
          </InputHidden>
          <InputHidden className="webtool.form.InputHidden" version="1.2.8">
           <externalID>3</externalID>
           <tag>AgAAAFAirjcWa9PEQQZ09r1xWccPsnqY6qv3GzYIrnwBcREG1pnpYoLQaP9A0UH9tQ18Vprj/z1jxTrdaCPzivQQ+MWW8kO+AwNsjHjwfAyx7o6Mrg==</tag>
           <beginLine>782</beginLine>
           <endLine>782</endLine>
          </InputHidden>
          <InputHidden className="webtool.form.InputHidden" version="1.2.8">
           <externalID>3</externalID>
           <tag>AgAAAFCIiVHLRcNjIZAYezCQ0bhsJZD3h+8hkNoViqHVxg+JOb0SQgaRPENtdfitVHijvvXHRxcmvIUfhcp0kEHCpw63OLMEmoV5LityfBniSww13g==</tag>
           <beginLine>783</beginLine>
           <endLine>783</endLine>
          </InputHidden>
          <InputHidden className="webtool.form.InputHidden" version="1.2.8">
           <externalID>3</externalID>
           <tag>AgAAAGAfEU+FYX+1tU9oDO+whHocIklUSXEB4FsUjiK3zbheFmlB7xmWo1KBqrGiqLT00Z9J6ag8UiDq83fJ5GQ9NDxQCudRevuNL70QLl7Zy18aVj0Mc1LxLNuEdoYM3kPAYfI=</tag>
           <beginLine>784</beginLine>
           <endLine>784</endLine>
          </InputHidden>
          <InputHidden className="webtool.form.InputHidden" version="1.2.8">
           <externalID>3</externalID>
           <tag>AgAAAFAO6uMjF/6HTVaebcaan6YJqnPzpSsZAYXehxVAsSKoxLpuK3ZQ+fG3JeZ/p+xybXknAu20LLwA/PMFAfZCFynwGbCV3cmHCHeFUJqxbQaUGw==</tag>
           <beginLine>785</beginLine>
           <endLine>785</endLine>
          </InputHidden>
          <InputText className="webtool.form.InputText" version="4.6.8">
           <externalID>9</externalID>
           <tag>AgAAANB/os3fN3xRU3No+LV7t6StuveJYZ4Z5G2CSkJ6ccH0URy1l7EzLQtZNHXTszLNyS5HBdKGnQt3ap/8nUYFII+FG6OZnWEMf3wSs+RahePTE9DVLtY6WHM/AkgO4SPTsmVdaYWKpjtCbchuStYAPB06shdMCaXUYblAPGSwvYsgQ+3CFIY8hbnCrK4wsV+BEoETc38/N47owmVm3SVRLBmfyrYotdLrXXKmYs4kr9iL04BpHfBjyljPH8K56BLcDGarzck77VeY052HKQE6wDqD</tag>
           <beginLine>787</beginLine>
           <endLine>787</endLine>
          </InputText>
          <InputText className="webtool.form.InputText" version="4.6.8">
           <externalID>9</externalID>
           <tag>AgAAAQCsUG2ZrgAl27VthTaEI/M2vO/tF5g2WZzbb++sNAa1ZaFxMy/sIrTvlUHK2JqEVY4ZmaReiUPSRhvYmz6NnLGquUp68B8Xg+uJHkwEaOKBPe6xexAmAk29eWK+rMBdutr3dlIPyHMRfRpbJmT0zfi02bI7+p5UGbanKTw4CHhnlq90aX1YGA26lAFbf0JSv1qWkqDvehYT4Wv7JcUQUpuXv4jy3+2+eik1qsdPNmk9v5zRQS5JTLdRfXzHdJmsO9mGZcyWmK6KD98CGayWDqTj6ST/nUURPJZG69Tr9fGxPmsiOXj17WAwGiDvgq5opDL3CyOrQlpCb03MWRU2z5nI</tag>
           <beginLine>788</beginLine>
           <endLine>788</endLine>
          </InputText>
          <InputText className="webtool.form.InputText" version="4.6.8">
           <externalID>9</externalID>
           <tag>AgAAAPCN/GXhKLiJ2HrTexa3eazTnO2fuCMsd7AtWuMLlPONyWJRfO+BkHXcoArC9G6VOjZVkWifSfEunt5evhF4/79U03ID6qNg8jrj+dtFMSzaTH5eSEJndA9I7Jq8O51Us/xus8WXshnzF5bLGaEGD/1RkkJad/2CiMXKDiuIeAVsN7fEGgiHznh6oBltLdvrB4CpgwQTHiWej8jiO1Py+yjhaXEX0AtSpDvn1pjQZMfGJRdFY0eiF83Rf6CJ3P4WR35Xm5L97Q42GLCScFYQq2Kp1qOCmMc7ks5kbVhNa74/4dKCpnvv2Ox3f7tfygmmySY=</tag>
           <beginLine>789</beginLine>
           <endLine>789</endLine>
          </InputText>
          <TextArea className="webtool.form.TextArea" version="6.6.8">
           <externalID>10</externalID>
           <tag>AgAAAKD9IXXQGH8XAs2D5x/DNpaQFck5BsWpismaNBKJlVqz2wAM3UD71OMvK4sJNOb7fPq1H93aqzPZRDaY9949uDsLc3q6BfTMfN12j9TVRfEsJgDhNpRCULWM4caWqCsCjT5xCFwiLm8BNUK0MhOJoqyKEfXPz5pvII7SmmXB7lG/NY0dn6KtgXE2xMufOgBRIf5PqGGm/5kaOebzNnZiyQRK</tag>
           <beginLine>790</beginLine>
           <endLine>790</endLine>
          </TextArea>
          <InputSubmit className="webtool.form.InputSubmit" version="5.2.8">
           <externalID>8</externalID>
           <tag>AgAAAGDFC+naJGLJKIdu1j/7goHsPNF6xUoBUe1LHZo0guAceExbKT4oSvt2DZSKyQZznwXBnLwZ7a0l6f0Mqv2io8d8j42yaDVk6EgFF7vy4IbJMuLrMt18Dp7EyXiMTzEvYbE=</tag>
           <beginLine>791</beginLine>
           <endLine>791</endLine>
           <isDefault>true</isDefault>
          </InputSubmit>
         </Form>
        </FormData>
        <keyID>3</keyID>
        <BooleanAuxData className="webtool.site.BooleanAuxData" version="1">
         <value>true</value>
        </BooleanAuxData>
        <MIMEType className="webtool.mime.MIMEType" version="1">
         <code>1</code>
        </MIMEType>
       </WebOther>
      </WebRoot>
      <name>rO0ABXA=</name>
      <Location className="webtool.site.Location" version="18">
       <followLinks>true</followLinks>
       <invokeMultipleCGI>true</invokeMultipleCGI>
       <passCookies>true</passCookies>
       <caseSensitive>true</caseSensitive>
       <ignoreRobots>true</ignoreRobots>
       <HTTPBase>/</HTTPBase>
       <httpDefault>*</httpDefault>
       <url>rO0ABXNyAAxqYXZhLm5ldC5VUkyWJTc2GvzkcgMAB0kACGhhc2hDb2RlSQAEcG9ydEwACWF1dGhvcml0eXQAEkxqYXZhL2xhbmcvU3RyaW5nO0wABGZpbGVxAH4AAUwABGhvc3RxAH4AAUwACHByb3RvY29scQB+AAFMAANyZWZxAH4AAXhw//////////90ABV3d3cuaW50ZWdyYXRpb25xYS5jb210AAEvcQB+AAN0AAVodHRwc3B4</url>
      </Location>
     </WebSite>
    </WebSiteCollectionManipulator>
    <Location className="webtool.site.Location" version="18">
     <followLinks>true</followLinks>
     <invokeMultipleCGI>true</invokeMultipleCGI>
     <passCookies>true</passCookies>
     <caseSensitive>true</caseSensitive>
     <ignoreRobots>true</ignoreRobots>
     <httpDefault>*</httpDefault>
     <url>rO0ABXNyAAxqYXZhLm5ldC5VUkyWJTc2GvzkcgMAB0kACGhhc2hDb2RlSQAEcG9ydEwACWF1dGhvcml0eXQAEkxqYXZhL2xhbmcvU3RyaW5nO0wABGZpbGVxAH4AAUwABGhvc3RxAH4AAUwACHByb3RvY29scQB+AAFMAANyZWZxAH4AAXhw//////////90ABV3d3cuaW50ZWdyYXRpb25xYS5jb210AABxAH4AA3QABWh0dHBzcHg=</url>
    </Location>
   </ScanningTool>
  </ToolTest>
 </TestSuite>
</SOAtestProject>
